# drone_video_converter

ffmpeg PyQt5 QWidget frontend for a very-very-very easy to use video converter for my dad's toy drone

## deps:
 - ffmpeg (command line tool)
 - python3
 - PyQt5

 - developed and tested only on ubuntu 18.04:

```
sudo apt-get install python3 python3-pyqt5 ffmpeg
```

## license 

 - GPLv3 (since PyQt5)

 - Many Thanks to https://codefisher.org/pastel-svg/ for the great icons! -> https://codefisher.org/pastel-svg/