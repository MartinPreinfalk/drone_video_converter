#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
    This file is part of drone_video_converter.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
'''

#  drone video converter for my dad's toy drone
#
# deps:
#   ffmpeg (command line tool)
#   PyQt5
#   python3
#
# sudo apt-get install python3 python3-pyqt5 ffmpeg

import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication, QFileDialog, QProgressDialog, QMessageBox, QLabel
from PyQt5.QtGui import QIcon
from PyQt5 import QtCore
import os
import subprocess
import threading
from time import sleep

class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.last_directory = ""
        self.last_result = None

    def initUI(self):
        self.textEdit = QTextEdit()
        self.textEdit.setReadOnly(True)
        self.setCentralWidget(self.textEdit)

        openAct = QAction(QIcon('res/open24.png'), 'Öffnen...', self)
        openAct.setShortcut('Ctrl+O')
        openAct.setStatusTip('Öffnen einer Datei zum Transcodieren')
        openAct.triggered.connect(self.transcode)

        exitAct = QAction(QIcon('res/exit24.png'), 'Beenden', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Beendet das Programm')
        exitAct.triggered.connect(self.close)

        self.statusBar()

        #menubar = self.menuBar()
        #fileMenu = menubar.addMenu('&Menü')
        #fileMenu.addAction(openAct)
        #fileMenu.addAction(exitAct)

        toolbar = self.addToolBar('Tools')
        toolbar.addAction(openAct)
        toolbar.addSeparator()
        toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 640, 480)
        self.setWindowTitle('Drohnen Video Transcoder')
        self.show()

    def transcode(self):

        # select files to transcode
        selection = QFileDialog.getOpenFileNames(self, 'Datei öffnen', self.last_directory)
        if not selection[0] or len(selection[0]) == 0:
            return
        self.last_directory = os.path.split(selection[0][0])[0]

        # setup progressbar
        progress = QProgressDialog("Transcodiere, bitte warten...", "Abbrechen", 0, 100, self)
        progress.setWindowModality(QtCore.Qt.WindowModal)
        progress.resize(500, 100)
        progress.setWindowTitle("Transcodiere, bitte warten...")
        progress.setValue(0)
        progress.setAutoReset(False);
        progress.setAutoClose(False);
        progress.show()
        progress.setValue(0.01)
        QtCore.QCoreApplication.instance().processEvents()

        # compute stats for progressbar
        p_est_size = [0]
        p_est_total = 0
        for input_file in selection[0]:
            p_est_total = os.stat(input_file).st_size
            p_est_size.append(p_est_total)

        # loop over all input files
        i = 0
        for input_file in selection[0]:
            output_file = os.path.splitext(input_file)[0] + "_transcoded.avi"

            #self.textEdit.append("<b>======================================================================</b>")
            self.textEdit.append("<hr></hr>")
            self.textEdit.append("<b>transcodiere {} nach {} ...</b>".format(input_file, output_file))
            self.textEdit.ensureCursorVisible()

            # init stats for progressbar
            p_start = p_est_size[i] / p_est_total * 100
            p_stop = p_est_size[i+1] / p_est_total * 100
            p_part = (p_stop - p_start)
            est_total = os.stat(input_file).st_size

            # spawn worker thread
            cmd = "ffmpeg -i {} -itsoffset 0.75 -i {} -map 1:v -map 0:a -vcodec mpeg4 -qscale:v 3  -y {}".format(
                input_file, input_file, output_file)
            w = threading.Thread(target=self.worker, args=(cmd, ))
            self.worker_running = True
            w.start()

            # update progressbar
            while self.worker_running and not progress.wasCanceled():
                sleep(0.5)
                try:
                    current_size = os.stat(output_file).st_size
                    p_current = p_start + current_size / est_total * p_part
                    if p_current < p_stop:
                        progress.setValue(p_current)
                        QtCore.QCoreApplication.instance().processEvents()
                except:
                    pass

            # handle user cancellation
            if progress.wasCanceled():
                progress.setWindowTitle("Abbruch...")
                progress.resize(500, 100)
                progress.setCancelButton(None)
                progress.setLabel(QLabel("bitte warten..."))
                progress.show()
                progress.setValue(0.01)
                QtCore.QCoreApplication.instance().processEvents()

                # update cancellation progress (waits for worker to exit) #FIXME support cancellation in worker
                while self.worker_running:
                    sleep(0.5)
                    try:
                        current_size = os.stat(output_file).st_size
                        p_current = current_size / est_total * 100
                        if p_current < 100:
                            progress.setValue(p_current)
                            QtCore.QCoreApplication.instance().processEvents()
                    except:
                        pass

                w.join()
                self.textEdit.append("<b>==> Abbruch beim Transcodieren von {} nach {}!</b>\n\n".format(input_file, output_file))
                self.textEdit.ensureCursorVisible()
                progress.hide()
                QMessageBox.warning(self, "Abbruch", "Nach Transcodierung von {} abgebrochen!".format(input_file), QMessageBox.Ok)
                return

            # join worker
            w.join()

            i+=1
            progress.setValue(p_stop)
            QtCore.QCoreApplication.instance().processEvents()

            self.textEdit.append(self.last_result.stdout.decode('utf-8'))
            self.textEdit.append(self.last_result.stderr.decode('utf-8'))
            self.textEdit.ensureCursorVisible()

            if self.last_result.returncode != 0:
                self.textEdit.append("<b>==> Fehler beim transcodieren von {} nach {}!</b>\n\n".format(input_file, output_file))
                progress.hide()
                self.textEdit.ensureCursorVisible()
                QMessageBox.critical(self, "Fehler", "Transcodierung von {} fehlgeschlagen!".format(input_file),
                                     QMessageBox.Ok)
                return

            self.textEdit.append("<b>==> {} wurde erfolgreich transcodiert nach {}!</b>\n\n".format(input_file, output_file))
            self.textEdit.ensureCursorVisible()

        progress.setValue(100)
        QtCore.QCoreApplication.instance().processEvents()
        progress.hide()

        QMessageBox.information(self, "Erfolgreich", "Transcodierung erfolgreich abgeschlossen!", QMessageBox.Ok)

    def worker(self, cmd):
        self.last_result = subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.worker_running = False

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
